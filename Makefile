AS=nasm -felf64
LD=ld
BINARY=executable.exe

ENV_DISABLE_ROP= LIB_DISABLE_ROP_MITIGATION=1
ENV=

all: buildall link

buildall:
	for sourcefile in ./*.s; do\
		$(ENV) $(AS) $${sourcefile} -o $${sourcefile}.o;\
		done;

link:
	$(LD) *.s.o -o $(BINARY)


clean:
	rm -rf *.o $(BINARY)

