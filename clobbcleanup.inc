; vim: ft=asm syntax=nasm

; NOTE: usage of CLOBBEREDCLEANUP
; shall reflect an option of making it a no-op
; therefore there should never be any reliance
; on it's effects
; NOTE: to disable ROP mitigation use
; environment variable LIB_DISABLE_ROP_MITIGATION
%macro CLOBBEREDCLEANUP 0-*
  %ifnenv LIB_DISABLE_ROP_MITIGATION
    %rep %0
      push %1
      %rotate 1
    %endrep

    xor rax, rax
    xor rcx, rcx
    xor rdi, rdi
    xor rdx, rdx
    xor rsi, rsi
    xor r8, r8
    xor r9, r9
    xor r10, r10
    xor r11, r11
    pxor	xmm0, xmm0
    pxor	xmm1, xmm1
    pxor	xmm2, xmm2
    pxor	xmm3, xmm3
    pxor	xmm4, xmm4
    pxor	xmm5, xmm5
    pxor	xmm6, xmm6
    pxor	xmm7, xmm7
    pxor	xmm8, xmm8
    pxor	xmm9, xmm9
    pxor	xmm10, xmm10
    pxor	xmm11, xmm11
    pxor	xmm12, xmm12
    pxor	xmm13, xmm13
    pxor	xmm14, xmm14
    pxor	xmm15, xmm15

    %rep %0
      %rotate -1
      pop %1
    %endrep
  %endif
%endmacro

