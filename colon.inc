; vim: ft=asm syntax=nasm
%define list_dictionary_start 0 

%macro colon 2 
	%%local_start:
		dq list_dictionary_start
		dq %2
		db %1, 0
		%define list_dictionary_start %%local_start
%endmacro

