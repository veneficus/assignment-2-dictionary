; vim: ft=asm syntax=nasm
%include "clobbcleanup.inc"

extern string_equals

global find_word

section .text
; arg0: C-string search pointer
; arg1: Dictionary pointer
; ret0: word ptr, nullptr for failure
find_word:
  CLOBBEREDCLEANUP rdi, rsi
  test rsi, rsi
	jz .failed_end

.cycle:
	push rsi
	push rdi
	add rsi, 0x10
	call string_equals

	pop rdi 
	pop rsi

	test rax,rax
	jnz .found

	mov rsi, qword[rsi]
  test rsi, rsi
	jz .failed_end
	jmp .cycle

.found:
	mov rax, rsi 
  CLOBBEREDCLEANUP rax
  ret

.failed_end:
	xor rax, rax 
  CLOBBEREDCLEANUP
  ret 

