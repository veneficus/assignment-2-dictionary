; vim: ft=asm syntax=nasm
%include "clobbcleanup.inc"

global exit
global print_newline
global print_string
global string_equals
global print_stream
global read_word

%assign SYSCALL_WRITE_NUMBER 1
%assign SYSCALL_NUMBER_EXIT 60
%assign STDIN 0
%assign STDOUT 1

%macro MACRO_SETUP_SYSCALL_ARGS1 1
  mov rdi, %1
%endmacro
%macro MACRO_SETUP_SYSCALL_ARGS2 2
  MACRO_SETUP_SYSCALL_ARGS1 %1
  mov rsi, %2
%endmacro
%macro MACRO_SETUP_SYSCALL_ARGS3 3
  MACRO_SETUP_SYSCALL_ARGS2 %1, %2
  mov rdx, %3
%endmacro


%macro MACRO_SETUP_SYSCALL_READ 3
  MACRO_SETUP_SYSCALL_ARGS3 %1, %2, %3
  xor rax, rax
  syscall
%endmacro

%macro MACRO_SETUP_SYSCALL_WRITE 3
  MACRO_SETUP_SYSCALL_ARGS3 %1, %2, %3
  mov rax, SYSCALL_WRITE_NUMBER
  syscall
%endmacro

%macro MACRO_SETUP_SYSCALL_EXIT 1
  MACRO_SETUP_SYSCALL_ARGS1 %1
  mov rax, SYSCALL_NUMBER_EXIT
  syscall
%endmacro

%define SYSCALL_READ(fd, buf, bytes) MACRO_SETUP_SYSCALL_READ fd, buf, bytes
%define SYSCALL_WRITE(fd, buf, bytes) MACRO_SETUP_SYSCALL_WRITE fd, buf, bytes
%define SYSCALL_EXIT(code) MACRO_SETUP_SYSCALL_EXIT code

section .text

; Принимает код возврата и завершает текущий процесс
; arg0: exit code
exit:
  CLOBBEREDCLEANUP rdi
  SYSCALL_EXIT(rdi)


; Принимает указатель на нуль-терминированную строку, возвращает её длину
; arg0: string pointer
string_length:
  CLOBBEREDCLEANUP rdi
	mov	rax, rdi
.string_length_loop:
	cmp	byte[rax], 0
	je .string_length_exit
	inc	rax
	jmp .string_length_loop
.string_length_exit:
	sub	rax, rdi
  CLOBBEREDCLEANUP rax
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; arg0: string pointer
print_string:
  CLOBBEREDCLEANUP rdi
  mov rsi, STDOUT
  call print_stream
  CLOBBEREDCLEANUP
  ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; arg0: string pointer
; arg1: fd id
print_stream:
  CLOBBEREDCLEANUP rdi, rsi
  push rsi
  push rdi
  call string_length
  pop rsi
  pop rdi
  
  SYSCALL_WRITE(rdi, rsi, rax)

  CLOBBEREDCLEANUP
  ret

; Принимает код символа и выводит его в stdout
; arg0: character
print_char:
  CLOBBEREDCLEANUP rdi
  movzx rdi, dil
  push rdi

  mov rdi, rsp
  call print_string

  pop rdi
  CLOBBEREDCLEANUP
  ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  CLOBBEREDCLEANUP
  mov rdi, `\n`
  call print_char
  CLOBBEREDCLEANUP
  ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
  CLOBBEREDCLEANUP rdi
  push rbx

  mov rax, rdi
  mov rcx, 10
  mov rdi, rsp
.print_uint_loop:
  xor rdx, rdx
  div rcx
  dec rsp
  add dl, '0'
  mov byte[rsp], dl
  test rax, rax
  jnz .print_uint_loop
  
  sub rdi, rsp
  mov rbx, rdi

  dec rsp
  mov byte[rsp], 0
  inc rsp

  mov rdi,rsp
  call print_string

  add rsp, rbx
  CLOBBEREDCLEANUP
  pop rbx
  ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
  CLOBBEREDCLEANUP rdi
  cmp rdi, 0
  jge .print_int_jumpover
  neg rdi
  push rdi
  mov rdi, '-'
  call print_char
  pop rdi
.print_int_jumpover:
  call print_uint
  CLOBBEREDCLEANUP
  ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; arg0: string pointer
; arg1: string pointer
string_equals:
	xor	rdx, rdx
.string_equals_loop:
	mov	al, byte[rdi+rdx]
	mov	cl, byte[rsi+rdx]
	cmp	al, cl
	jne	.string_equals_exit
	test	al, al
	je	.string_equals_exit
	inc	rdx
	jmp	.string_equals_loop
.string_equals_exit:
	or	al, cl
	sete	al
	movzx	rax, al
	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  dec rsp

  SYSCALL_READ(STDIN,rsp,1)

  or ah, al
  mov al, byte [rsp]
  inc rsp
  ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; arg0: buffer pointer
; arg1: buffer size
; ret0: buffer pointer, 0 in case of failure
; ret1: word size     , 0 in case of failure
read_word:
  CLOBBEREDCLEANUP rdi, rsi
  xor r10, r10; length accumulator

.read_word_whitespace_sink:
  push rdi
  push rsi
  call read_char
  pop rsi
  pop rdi
  test ah, ah
  jz .read_word_exit
  cmp al, 0x20
  je .read_word_whitespace_sink
  cmp al, 0x0A
  je .read_word_whitespace_sink
  cmp al, 0x09
  je .read_word_whitespace_sink

  inc r10
  cmp r10, rsi
  jge .overflow
  mov byte[rdi+r10-1], al

  .read_word_loop:
  push rdi
  push rsi
  call read_char
  pop rsi
  pop rdi
  test ah, ah
  jz .read_word_exit
  cmp al, 0x20
  je .read_word_exit
  cmp al, 0x0A
  je .read_word_exit
  cmp al, 0x09
  je .read_word_exit

  inc r10
  cmp r10, rsi
  je .overflow
  mov byte[rdi+r10-1], al
  jmp .read_word_loop

.read_word_exit:
  mov byte[rdi+r10], 0
  mov rax, rdi
  mov rdx, r10
  CLOBBEREDCLEANUP rax, rdx
  ret

.overflow:
  xor rax, rax
  xor rdx, rdx
  CLOBBEREDCLEANUP
  ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; arg0: string pointer
; ret0: number
; ret1: number of characters of the number
parse_uint:
  CLOBBEREDCLEANUP rdi
  xor rcx, rcx
.parse_uint_loop1:
  cmp byte[rdi+rcx], '0'
  jge .digit_validity_check
  test rcx, rcx
  jnz .parse_uint_encountered_null

  xor rax, rax
  xor rdx, rdx
  CLOBBEREDCLEANUP
  ret

.valid_digit_continue:
  inc rcx
  jmp .parse_uint_loop1

.digit_validity_check:
  cmp byte[rcx+rdi], '9'
  jbe .valid_digit_continue
  test rcx, rcx
  jnz .parse_uint_encountered_null
  xor rax, rax
  xor rdx, rdx
  CLOBBEREDCLEANUP rax, rdx
  ret

.parse_uint_encountered_null:
  xor r10,r10
  mov rsi, 1
  push rcx
  .parse_uint_loop2:
  xor rax, rax

  mov al, byte[rdi+rcx-1]
  sub al, '0'
  mul rsi
  add r10, rax

  shl rsi, 1
  lea rsi, [rsi*4+rsi]
  dec rcx
  jnz .parse_uint_loop2

  pop rdx
  mov rax, r10
  CLOBBEREDCLEANUP rax, rdx
  ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
  CLOBBEREDCLEANUP rdi
  cmp byte[rdi], '-'
  jz .negative
  call parse_uint
  CLOBBEREDCLEANUP rax, rdx
  ret
.negative:
  inc rdi
  call parse_uint
  test rdx, rdx
  jz .notanumber
  inc rdx ; sign
  neg rax
  CLOBBEREDCLEANUP rax, rdx
  ret
.notanumber:
  xor rax, rax
  xor rdx, rdx
  CLOBBEREDCLEANUP
  ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; arg0: input string pointer
; arg1: output string pointer
; arg2: output string size
string_copy:
  push rdi
  push rsi
  push rdx
	call string_length
  pop rdx
  pop rsi
  pop rdi

  inc rax
	cmp rax, rdx
	jg  .string_copy_failed

	xor rdx, rdx
.string_copy_loop:
	cmp rdx, rax
	je	.string_copy_succes
	mov	cl, byte[rdi+rdx]
	mov	byte[rsi+rdx], cl
	inc	rdx
	jmp	.string_copy_loop

.string_copy_succes:
  CLOBBEREDCLEANUP rax
	ret
.string_copy_failed:
  CLOBBEREDCLEANUP
  xor rax, rax
	ret

