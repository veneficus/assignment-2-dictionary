; vim: ft=asm syntax=nasm
%include "clobbcleanup.inc"
%include "words.inc"

extern exit
extern find_word
extern print_newline
extern print_stream
extern print_string
extern read_word

%assign STDOUT 1
%assign STDERR 2

global _start

section .rodata
	ERROR_MESSAGE db `Invalid input\n\0`
	ERROR_UNREAD_MESSAGE db `Input reading error\n\0`

section .text 
_start:
  sub rsp, 0x100

  xor rax, rax ; syscall: read
  xor rdi, rdi ; fd: stdin
  mov rsi, rsp ; buf
  mov rdx, 0xff ; count
  syscall
  test rax, rax
  jz .reading_error
  mov byte[rsp+rax-1], 0

  lea rcx, [rax+1]
  lea rsi, [rsp+rax]
  lea rdi, [rsp+0x100]
  std
  rep movsb

  mov rdi, rsp
  mov rsi, list_dictionary_start 
  call find_word
  test rax, rax
  jz .corrupted

  add rax, 8
  mov rdi, [rax]
  call print_string
  call print_newline
  xor rdi, rdi 
  jmp exit

.corrupted:
	mov rdi, ERROR_MESSAGE
  mov rsi, STDERR
	call print_stream
  jmp .exit_thunk

.reading_error:
	mov rdi, ERROR_UNREAD_MESSAGE
  mov rsi, STDERR
	call print_stream
  jmp .exit_thunk

.exit_thunk:
	mov rdi, 1
	jmp exit 

