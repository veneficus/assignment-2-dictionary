#!/bin/sh
# Was tested with dash

make clean
make

failed_expected="Invalid input"
expected1="testing 1"
expected2="testing 2"
expected3="testing 3"
expected4="fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"

run_failed_input="doesnotexist"
run1_input="test1"
run2_input="test2"
run3_input="test3"
run4_input="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

run_failed=$(echo "${run_failed_input}" | ./executable.exe 2>&1)
run1=$(echo "${run1_input}" | ./executable.exe)
run2=$(echo "${run2_input}" | ./executable.exe)
run3=$(echo "${run3_input}" | ./executable.exe)
run4=$(echo "${run4_input}" | ./executable.exe)

if [ "${failed_expected}" != "${run_failed}" ];
then
  echo "Test0 failed"
  exit 1
else
  echo "Test0 success"
fi

if [ "${expected1}" != "${run1}" ];
then
  echo "Test1 failed"
  exit 1
else
  echo "Test1 success"
fi

if [ "${expected2}" != "${run2}" ];
then
  echo "Test2 failed"
  exit 1
else
  echo "Test2 success"
fi

if [ "${expected3}" != "${run3}" ];
then
  echo "Test3 failed"
  exit 1
else
  echo "Test3 success"
fi

if [ "${expected4}" != "${run4}" ];
then
  echo "Test4 failed"
  exit 1
else
  echo "Test4 success"
fi

echo "All tests succeeded"
